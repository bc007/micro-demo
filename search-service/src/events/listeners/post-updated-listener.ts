import { Message } from "node-nats-streaming";
import searchService from "../../service/search-service";
import { Listener } from "../base-listener";
import { PostCreatedEvent } from "../post-created-event";
import { PostUpdatedEvent } from "../post-updated-event";
import { Subjects } from "../subjects";

export class PostUpdatedListener extends Listener<PostUpdatedEvent> {
  subject: Subjects.PostUpdated = Subjects.PostUpdated;
  queueGroupName = "search-service";

  async onMessage(data: PostUpdatedEvent["data"], message: Message): Promise<void> {
    console.log("Event received", data);
    await searchService.updatePost({...data}, data._id);
    console.log("Post updated event completed");
    message.ack();
  }
}
