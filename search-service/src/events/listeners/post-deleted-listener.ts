import { Message } from "node-nats-streaming";
import searchService from "../../service/search-service";
import { Listener } from "../base-listener";
import { PostDeletedEvent } from "../post-deleted-event";
import { Subjects } from "../subjects";

export class PostDeletedListener extends Listener<PostDeletedEvent> {
  subject: Subjects.PostDeleted = Subjects.PostDeleted;
  queueGroupName = "search-service";

  async onMessage(data: PostDeletedEvent["data"], message: Message): Promise<void> {
    console.log("Event received", data);
    await searchService.deletePost(data._id);
    console.log("Post deleted event completed");
    message.ack();
  }
}
