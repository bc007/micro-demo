import { Router } from "express";
import searchController from "../controller/search-controller";
import { auth } from "../middleware/auth-middleware";

const router = Router();

router.get("", auth, searchController.search);

export { router as SearchRouter };