import mongoose from "mongoose";

const searchPostsSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    }
  },
  { timestamps: true, _id: false }
);

const SearchPost = mongoose.model("SearchPost", searchPostsSchema);

export default SearchPost;
