import cookieParser from "cookie-parser";
import express, { Request, Response } from "express";
import initDB from "./database";
import { PostCreatedListener } from "./events/listeners/post-created-listener";
import { PostDeletedListener } from "./events/listeners/post-deleted-listener";
import { PostUpdatedListener } from "./events/listeners/post-updated-listener";
import Handler from "./exceptions";
import natsWrapper from "./nats-wrapper";
import { SearchRouter } from "./routes/search-routes";

const app = express();

app.use(express.json());
app.use(cookieParser());

// routes
app.use("/api/search", SearchRouter);

const PORT = process.env.PORT || 3000;

app.get("/api/search/status", (request: Request, response: Response) => {
  return response.status(200).json({ message: "OK" });
});

// handle errors
app.use("*/", Handler.handleError);

async function start() {
  try {
    await natsWrapper.connect("ticketing", "agjhadhadaass", "http://nats-srv:4222");

    // listen for events
    const postCreatedListener = new PostCreatedListener(natsWrapper.client);
    postCreatedListener.listen();

    const postUpdatedListener = new PostUpdatedListener(natsWrapper.client);
    postUpdatedListener.listen();

    const postDeletedListener = new PostDeletedListener(natsWrapper.client);
    postDeletedListener.listen(); 

    natsWrapper.client.on("close", () => {
      console.log("NATS connection closed!");
      process.exit();
    });

    process.on("SIGINT", () => natsWrapper.client.close());
    process.on("SIGTERM", () => natsWrapper.client.close());

    initDB("mongodb://search-mongo-srv:27017/search");

    app.listen(PORT, () => {
      console.log(`Search service is listening in http://localhost:${PORT}`);
    });
  } catch (error) {
    console.error(error);
  }
}

start();
