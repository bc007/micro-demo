import { PostCreatedEvent } from "../events/post-created-event";
import UnAuthorizedError from "../exceptions/unauthorized-error";
import ValidatorError from "../exceptions/validator-error";
import SearchPost from "../models/search-post";
import { IPost, ISavePostDTO } from "../types";

class SearchService {
  async searchPosts(query: string): Promise<ISavePostDTO[]> {
    try {
      const posts: ISavePostDTO[] = await SearchPost.find({
        $or: [
          {
            title: {
              $regex: query,
              $options: "i",
            },
          },
          {
            body: {
              $regex: query,
              $options: "i",
            },
          },
        ],
      });

      return posts;
    } catch (error) {
      throw error;
    }
  }

  async savePost(post: PostCreatedEvent["data"]): Promise<IPost> {
    try {
      const postObj = new SearchPost({
        _id: post._id,
        title: post.title,
        body: post.body,
      });
      const savedPost = await postObj.save();
      return savedPost;
    } catch (error) {
      throw error;
    }
  }

  async updatePost(
    post: PostCreatedEvent["data"],
    postId: string,
  ): Promise<IPost> {
    try {
      const retrievedPost = await SearchPost.findOne({ _id: postId });
      if (!retrievedPost) {
        throw new ValidatorError("No post found with this Id");
      }
      retrievedPost.title = post.title;
      retrievedPost.body = post.body;
      await retrievedPost.save();
      return retrievedPost;
    } catch (error) {
      throw error;
    }
  }

  async deletePost(postId: string): Promise<boolean> {
    try {
      const retrievedPost = await SearchPost.findOne({ _id: postId });
      if (!retrievedPost) {
        throw new ValidatorError("No post found with this Id");
      }
      await retrievedPost.delete();
      return true;
    } catch (error) {
      throw error;
    }
  }
}

export default new SearchService();
