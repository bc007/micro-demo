import { NextFunction, Response, Request } from "express";
import ValidatorError from "../exceptions/validator-error";
import searchService from "../service/search-service";
import { ISavePostDTO, ResponseDTO, statusCode } from "../types";

class SearchController {
  async search(
    req: Request,
    res: Response<ResponseDTO<ISavePostDTO[]>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<ISavePostDTO[]>> | void> {
    try {
      const { query } = req.query;

      if (!query) {
        throw new ValidatorError("You should need add a query parameter"); 
      }

      const posts = await searchService.searchPosts(query?.toString()!);
      const response = new ResponseDTO<ISavePostDTO[]>(
        statusCode.OK,
        true,
        posts,
        null
      );
      return res.status(statusCode.OK).json(response);
    } catch (error) {
      return next(error);
    }
  }
}

export default new SearchController();
