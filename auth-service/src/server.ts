import express, { Request, Response } from "express";
import cookieParser from "cookie-parser";
import initDB from "./database";
import Handler from "./exceptions";
import { AuthRouter } from "./routes/auth-routes";

const app = express();

app.use(express.json());
app.use(cookieParser());

// routes
app.use("/api/users", AuthRouter);

const PORT = process.env.PORT || 3000;

app.get("/api/users/status", (request: Request, response: Response) => {
    return response.status(200).json({ message: "OK" });
});

// handle errors
app.use("*/", Handler.handleError);


initDB("mongodb://auth-mongo-srv:27017/auth");

app.listen(PORT, () => {
    console.log(`Auth service is listening in http://localhost:${PORT}`);
});


