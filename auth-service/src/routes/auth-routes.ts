import { Router } from "express";
import authController from "../controller/auth-controller";
import { auth } from "../middleware/auth-middleware";

const router: Router = Router();

router.post("/register", authController.register);
router.post("/login", authController.login);
router.get("/current-user", auth, authController.getCurrentUser);


export { router as AuthRouter };