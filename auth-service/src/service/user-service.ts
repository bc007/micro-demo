import ValidatorError from "../exceptions/validator-error";
import User from "../models/user";
import { ISignUpDTO, IUser } from "../types";
import bcrypt from "bcrypt";

class UserService {
  async getCurrentUser(email: string): Promise<IUser> {
    try {
      const user: IUser = await this.findUserByEmail(email);
      if (!user) {
        throw new ValidatorError("User with this email not found");
      }
      return user;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async saveUser(user: ISignUpDTO): Promise<IUser> {
    try {
      const retrivedUser: IUser = await this.findUserByEmail(user.email);
      if (retrivedUser) {
        throw new ValidatorError("User with this email already exists.");
      }
      const hasedPassword: string = await bcrypt.hash(user.password, 6);
      const userToBeSaved = new User({
        email: user.email,
        password: hasedPassword,
      });
      const savedUser: IUser = await userToBeSaved.save();

      return savedUser;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async findUserByEmail(email: string): Promise<IUser> {
    try {
      const user = await User.findOne({ email });
      return user;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}

export default new UserService();
