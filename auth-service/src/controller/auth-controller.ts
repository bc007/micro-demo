import { NextFunction, Request, Response } from "express";
import ValidatorError from "../exceptions/validator-error";
import userService from "../service/user-service";
import { IUser, ResponseDTO, statusCode } from "../types";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

class AuthController {
  async register(
    req: Request,
    res: Response<ResponseDTO<IUser>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<IUser>> | void> {
    try {
      const { email, password } = req.body;
      const savedUser = await userService.saveUser({ email, password });
      const response = new ResponseDTO<IUser>(
        statusCode.CREATED,
        true,
        savedUser,
        null
      );
      return res.status(statusCode.CREATED).json(response);
    } catch (error) {
      return next(error);
    }
  }

  async login(
    req: Request,
    res: Response<ResponseDTO<{ accessToken: string }>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<{ accessToken: string }>> | void> {
    try {
      const { email, password } = req.body;
      // find user
      const user: IUser = await userService.findUserByEmail(email);
      
      if (!user) {
        throw new ValidatorError("Email/password not valid");
      }

      // check password
      const isPasswordMatch = await bcrypt.compare(password, user.password);

      if (!isPasswordMatch) {
        throw new ValidatorError("Email/password not valid");
      }

      // generate token and login
      const token = await jwt.sign({ email: user.email, _id: user._id }, "mySecret");
      res.cookie("access_token", token, {
        httpOnly: true,
        domain: "talkfree.dev",
        path: "/",
        sameSite: true,
        secure: process.env.NODE_ENV === "production",
      });

      const response = new ResponseDTO<{ accessToken: string }>(
        statusCode.OK,
        true,
        { accessToken: token },
        null
      );
      return res.status(statusCode.CREATED).json(response);
    } catch (error) {
      return next(error);
    }
  }

  getCurrentUser(
    req: Request,
    res: Response<ResponseDTO<IUser>>,
    next: NextFunction
  ): Response<ResponseDTO<IUser>> | void {
    try {
      const user: IUser = req.user!;
      const response = new ResponseDTO<IUser>(statusCode.OK, true, user, null);
      return res.status(statusCode.OK).json(response);
    } catch (error) {
      return next(error);
    }
  }
}

export default new AuthController();
