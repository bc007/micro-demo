import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import UnAuthenticatedError from "../exceptions/unauthenticated-error";


export const auth = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    const accessToken = req.cookies["access_token"];
    if (!accessToken) {
      throw new UnAuthenticatedError("Token not found");
    }
    const decodedValue: any = jwt.verify(
      accessToken,
      "mySecret",
    ) as any;

    // check user
    req.user = decodedValue;
    return next();
  } catch (error) {
    return next(error);
  }
};
