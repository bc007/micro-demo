import express, { Request, Response } from "express";
import cookieParser from "cookie-parser";
import initDB from "./database";
import Handler from "./exceptions";
import natsWrapper from "./nats-wrapper";
import { PostCreatedListener } from "./events/listeners/post-created-listener";

const app = express();

app.use(express.json());
app.use(cookieParser());

// routes

const PORT = process.env.PORT || 3000;

app.get("/api/comments/status", (request: Request, response: Response) => {
  return response.status(200).json({ message: "OK" });
});

// handle errors
app.use("*/", Handler.handleError);

async function start() {
  await natsWrapper.connect("ticketing", "alsdkj", "http://nats-srv:4222");
  
  const listener = new PostCreatedListener(natsWrapper.client);
  listener.listen();
  
  natsWrapper.client.on("close", () => {
    console.log("NATS connection closed!");
    process.exit();
  });
  process.on("SIGINT", () => natsWrapper.client.close());
  process.on("SIGTERM", () => natsWrapper.client.close());

  initDB("mongodb://comments-mongo-srv:27017/auth");

  app.listen(PORT, () => {
    console.log(`Comments service is listening in http://localhost:${PORT}`);
  });
}

start();
