import { Subjects } from './subjects';

export interface PostCreatedEvent {
  subject: Subjects.PostCreated;
  data: {
    _id: string;
    title: string;
    body: string;
  };
}
