import { Message } from "node-nats-streaming";
import { Listener } from "../base-listener";
import { PostCreatedEvent } from "../post-created-event";
import { Subjects } from "../subjects";

export class PostCreatedListener extends Listener<PostCreatedEvent> {
  subject: Subjects.PostCreated = Subjects.PostCreated;
  queueGroupName = "comments-service";

  onMessage(data: PostCreatedEvent["data"], message: Message): void {
    console.log("Event received", data);
    message.ack();
  }
}
