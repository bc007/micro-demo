import mongoose from "mongoose";

const commentsSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true,
    },
    postId: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    }, 
  },
  { timestamps: true }
);

const User = mongoose.model("User", commentsSchema);

export default User;
