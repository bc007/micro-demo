export default class UnAuthorizedError extends Error {
    message: string;
    constructor(message: string) {
      super();
      this.message = message;
    }
  }
  