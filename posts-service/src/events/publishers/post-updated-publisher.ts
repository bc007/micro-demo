import { Publisher } from "../base-publisher";
import { PostUpdatedEvent } from "../post-updated-event";
import { Subjects } from "../subjects";

export class PostUpdatedPublisher extends Publisher<PostUpdatedEvent> {
  readonly subject = Subjects.PostUpdated;
}
