import { Publisher } from "../base-publisher";
import { PostDeletedEvent } from "../post-deleted-event";
import { Subjects } from "../subjects";

export class PostDeletedPublisher extends Publisher<PostDeletedEvent> {
  readonly subject = Subjects.PostDeleted;
}
