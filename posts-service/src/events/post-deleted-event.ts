import { Subjects } from './subjects';

export interface PostDeletedEvent {
  subject: Subjects.PostDeleted;
  data: {
    _id: string;
  };
}
