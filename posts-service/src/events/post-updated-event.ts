import { Subjects } from './subjects';

export interface PostUpdatedEvent {
  subject: Subjects.PostUpdated;
  data: {
    _id: string;
    title: string;
    body: string;
  };
}
