import mongoose from "mongoose";

const postsSchema = new mongoose.Schema(
  {
    title: {
        type: String,
        required: true,
    },
    body: {
        type: String,
        required: true,
    },
    userId: {
        type: String,
        required: true,
    }
  },
  { timestamps: true }
);

const Post = mongoose.model("Post", postsSchema);

export default Post;