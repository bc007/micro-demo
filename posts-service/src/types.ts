export interface IUser {
  _id: string;
  email: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface IPost {
  _id: string;
  title: string;
  body: string;
  userId:  string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ISavePostDTO {
  title: string;
  body: string;
}

export interface ISignUpDTO {
  email: string;
  password: string;
}

export class ResponseDTO<T> {
  statusCode: number;
  success: boolean;
  data: T | null;
  error: string | [] | null;

  constructor(
    statusCode: number,
    success: boolean,
    data: T | null,
    error: string | [] | null
  ) {
    this.statusCode = statusCode;
    this.success = success;
    this.data = data;
    this.error = error;
  }
}
export enum statusCode {
  OK = 200,
  CREATED = 201,
  BAD_REQUEST = 400,
  INTERNAL_SERVER_ERROR = 500,
  UNAUTHENTICATED = 401,
  UNAUTHORIZED = 403,
}

export interface ITokenPayload {
  email: string;
}

declare global {
  namespace Express {
    interface Request {
      user?: IUser;
    }
  }
}
