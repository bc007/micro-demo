import cookieParser from "cookie-parser";
import express, { Request, Response } from "express";
import initDB from "./database/index";
import { PostCreatedPublisher } from "./events/publishers/post-created-publisher";
import Handler from "./exceptions/index";
import natsWrapper from "./nats-wrapper";
import { PostRouter } from "./routes/posts-routes";

const app = express();

app.use(express.json());
app.use(cookieParser());

// routes
app.use("/api/posts", PostRouter);

const PORT = process.env.PORT || 3000;

app.get("/api/posts/status", (request: Request, response: Response) => {
  return response.status(200).json({ message: "OK" });
});

// handle errors
app.use("*/", Handler.handleError);

async function start() {
  try {
    await natsWrapper.connect("ticketing", "agjhadhadaa", "http://nats-srv:4222");

    natsWrapper.client.on("close", () => {
      console.log("NATS connection closed!");
      process.exit();
    });

    process.on("SIGINT", () => natsWrapper.client.close());
    process.on("SIGTERM", () => natsWrapper.client.close()); 

    initDB("mongodb://posts-mongo-srv:27017/posts");

    app.listen(PORT, () => {
      console.log(`Posts service is listening in http://localhost:${PORT}`);
    });
  } catch (error) {
    console.error(error);
  }
}

start();
