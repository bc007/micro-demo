import { PostCreatedPublisher } from "../events/publishers/post-created-publisher";
import { PostDeletedPublisher } from "../events/publishers/post-deleted-publisher";
import { PostUpdatedPublisher } from "../events/publishers/post-updated-publisher";
import UnAuthorizedError from "../exceptions/unauthorized-error";
import UnAuthorized from "../exceptions/unauthorized-error";
import ValidatorError from "../exceptions/validator-error";
import Post from "../models/post";
import natsWrapper from "../nats-wrapper";
import { IPost, ISavePostDTO } from "../types";

class PostService {
  async savePost(post: ISavePostDTO, userId: string): Promise<IPost> {
    try {
      const postObj = new Post({
        userId: userId,
        title: post.title,
        body: post.body,
      });
      const savedPost = await postObj.save();

      const publisher = new PostCreatedPublisher(natsWrapper.client);
      publisher.publish({
        _id: savedPost._id,
        title: savedPost.title,
        body: savedPost.body,
      });
      return savedPost;
    } catch (error) {
      throw error;
    }
  }

  async updatePost(
    post: ISavePostDTO,
    postId: string,
    userId: string
  ): Promise<IPost> {
    try {
      const retrievedPost = await Post.findOne({ _id: postId });
      if (!retrievedPost) {
        throw new ValidatorError("No post found with this Id");
      }
      if (retrievedPost.userId !== userId) {
        throw new UnAuthorizedError("You cannot perform this operation");
      }
      retrievedPost.title = post.title;
      retrievedPost.body = post.body;
      retrievedPost.save();

      const publisher = new PostUpdatedPublisher(natsWrapper.client);
      publisher.publish({
        _id: retrievedPost._id,
        title: retrievedPost.title,
        body: retrievedPost.body,
      });
      return retrievedPost;
    } catch (error) {
      throw error;
    }
  }

  async deletePost(postId: string, userId: string): Promise<boolean> {
    try {
      const retrievedPost = await Post.findOne({ _id: postId });
      if (!retrievedPost) {
        throw new ValidatorError("No post found with this Id");
      }
      if (retrievedPost.userId !== userId) {
        throw new UnAuthorizedError("You cannot perform this operation");
      }
      await retrievedPost.delete();
      const publisher = new PostDeletedPublisher(natsWrapper.client);
      publisher.publish({
        _id: retrievedPost._id,        
      });
      return true;
    } catch (error) {
      throw error;
    }
  }

  async getAllPosts(): Promise<IPost[]> {
    try {
      const posts = await Post.find().sort("-createdAt");
      return posts;
    } catch (error) {
      throw error;
    }
  }
}

export default new PostService();
