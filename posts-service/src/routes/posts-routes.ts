import { Router } from "express";
import { auth } from "../middleware/auth-middleware";
import postsController from "../controller/posts-controller";

const router = Router();

router.post("/create", auth, postsController.save);
router.put("/:_id", auth, postsController.update);
router.delete("/:_id", auth, postsController.delete);
router.get("", auth, postsController.getAllPosts);

export { router as PostRouter };