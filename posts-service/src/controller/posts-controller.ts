import { NextFunction, Request, Response } from "express";
import postsService from "../service/posts-service";
import { IPost, ISavePostDTO, IUser, ResponseDTO, statusCode } from "../types";

class PostController {
  async save(
    req: Request,
    res: Response<ResponseDTO<IPost>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<IPost>> | void> {
    try {
      const user: IUser = req.user!;
      const post: ISavePostDTO = req.body;
      const savedPost: IPost = await postsService.savePost(post, user._id);
      const response = new ResponseDTO<IPost>(
        statusCode.CREATED,
        true,
        savedPost,
        null
      );
      return res.status(statusCode.CREATED).json(response);
    } catch (error) {
      return next(error);
    }
  }

  async update(
    req: Request,
    res: Response<ResponseDTO<IPost>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<IPost>> | void> {
    try {
      const { _id } = req.params;
      const post: ISavePostDTO = req.body;
      const updatedPost: IPost = await postsService.updatePost(
        post,
        _id,
        req.user!._id
      );
      const response = new ResponseDTO<IPost>(
        statusCode.OK,
        true,
        updatedPost,
        null
      );
      return res.status(statusCode.OK).json(response);
    } catch (error) {
      return next(error);
    }
  }

  async delete(
    req: Request,
    res: Response<ResponseDTO<boolean>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<boolean>> | void> {
    try {
      const { _id } = req.params;
      const isDeleted: boolean = await postsService.deletePost(
        _id,
        req.user!._id
      );
      const response = new ResponseDTO<boolean>(
        statusCode.OK,
        true,
        isDeleted,
        null
      );
      return res.status(statusCode.OK).json(response);
    } catch (error) {
      return next(error);
    }
  }

  async getAllPosts(
    req: Request,
    res: Response<ResponseDTO<IPost[]>>,
    next: NextFunction
  ): Promise<Response<ResponseDTO<IPost[]>> | void> {
    try {
      const posts = await postsService.getAllPosts();
      const response = new ResponseDTO<IPost[]>(
        statusCode.OK,
        true,
        posts,
        null
      );
      return res.status(statusCode.OK).json(response);
    } catch (error) {
      return next(error);
    }
  }
}

export default new PostController();
